<?php
/*
Template Name: Front
*/
get_header(); ?>

<div class="banner">
    <img src="http://reggaefestival.nl/wp-content/uploads/2017/12/BANNER-GOEIEKLEUR.jpg" alt="<?php bloginfo( 'name' ); ?>">
</div>

<section id="line-up" class="section">
    
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-12 medium-4">
                <h2 class="section__title text-uppercase">About the Festival</h2>
            </div>
            <div class="cell small-12 medium-8">
                <h3>Second Open Air Reggae Festival</h3>
                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti hic accusamus, itaque maxime suscipit repellendus earum reprehenderit beatae alias a vitae totam, accusantium omnis dicta! Reiciendis necessitatibus recusandae porro eveniet amet non suscipit, exercitationem sequi quibusdam placeat ex libero labore dolores accusamus, quo dignissimos dolorem, nihil illo. Nesciunt numquam laborum temporibus iste at voluptatum rem odit rerum aperiam ex possimus, eius dolores neque architecto, libero tenetur, molestias ad totam! Ab praesentium, doloribus qui deserunt id impedit reiciendis doloremque nemo dolor fuga illum amet voluptatibus et dignissimos incidunt rerum, fugiat culpa. Quam doloremque totam quas sit autem consequuntur necessitatibus modi incidunt obcaecati, optio vel, ducimus facilis fugit est dolores non distinctio ex dolor tempora magnam quos! Voluptas repellat, ipsum, aspernatur velit veniam est voluptate veritatis sed! Harum voluptatum neque, cupiditate odit labore ipsa ex dolores tenetur porro minus adipisci explicabo rerum. Voluptatibus, cum, dignissimos! Magni enim architecto ex cupiditate. Repellat, blanditiis.</p>
                <h4>Bigger better & and more to eat!</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, deserunt placeat officia pariatur error, laudantium ullam, distinctio dignissimos cum ipsum delectus! Consectetur, ipsum, minima. Quo delectus deleniti, minima, esse blanditiis iste, provident quaerat illum minus molestiae cum eveniet beatae quasi. Temporibus, voluptate. Dolorem inventore sit, esse temporibus libero corporis eaque fugit totam nostrum quis? Consectetur eligendi vitae, velit saepe modi placeat laborum cupiditate iusto illum excepturi odio, natus nobis quis dicta, soluta cum quae similique. Doloremque laborum quod repellat nulla.</p>
            </div>
        </div>
    </div>
    
    
</section>

<section id="line-up" class="section">
    
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-12 medium-4">
                <h2 class="section__title text-uppercase">Line-Up</h2>
            </div>
            <div class="cell small-12 medium-8">
                <div class=" grid-x grid-padding-x">
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/d/f/csm_Akeem_c8393223d5.jpg">
                            <div class="card-section">
                                <h4>A#KEEM </h4>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/4/e/csm_abajonai_ba9ae46a05.jpg">
                            <div class="card-section">
                                <h4>ABAJONAI</h4>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/d/f/csm_Akeem_c8393223d5.jpg">
                            <div class="card-section">
                                <h4>A#KEEM </h4>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/4/e/csm_abajonai_ba9ae46a05.jpg">
                            <div class="card-section">
                                <h4>ABAJONAI</h4>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/b/8/csm_abyssinians_50da0a230a.jpg">
                            <div class="card-section">
                                <h4>ABYSSINIANS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-12 medium-6 large-4">
                        <div class="card">
                            <img class="card__image" src="https://www.reggaeville.com/fileadmin/_processed_/b/8/csm_abyssinians_50da0a230a.jpg">
                            <div class="card-section">
                                <h4>ABYSSINIANS</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>

<section id="line-up" class="section">
    
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-12 medium-4">
                <h2 class="section__title text-uppercase">Get in touch</h2>
            </div>
            <div class="cell small-12 medium-8">
                <h3>Questions? Get in touch!</h3>
                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti hic accusamus, itaque maxime suscipit repellendus earum reprehenderit beatae alias a vitae totam, accusantium omnis dicta! Reiciendis necessitatibus recusandae porro eveniet amet non suscipit, exercitationem sequi quibusdam placeat ex libero labore dolores accusamus, quo dignissimos dolorem, nihil illo. Nesciunt numquam laborum temporibus iste at voluptatum rem odit rerum aperiam ex possimus, eius dolores neque architecto, libero tenetur, molestias ad totam! Ab praesentium, doloribus qui deserunt id impedit reiciendis doloremque nemo dolor fuga illum amet voluptatibus et dignissimos incidunt rerum, fugiat culpa. Quam doloremque totam quas sit autem consequuntur necessitatibus modi incidunt obcaecati, optio vel, ducimus facilis fugit est dolores non distinctio ex dolor tempora magnam quos! Voluptas repellat, ipsum, aspernatur velit veniam est voluptate veritatis sed! Harum voluptatum neque, cupiditate odit labore ipsa ex dolores tenetur porro minus adipisci explicabo rerum. Voluptatibus, cum, dignissimos! Magni enim architecto ex cupiditate. Repellat, blanditiis.</p>
                <h4>Follow us on social media</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, deserunt placeat officia pariatur error, laudantium ullam, distinctio dignissimos cum ipsum delectus! Consectetur, ipsum, minima. Quo delectus deleniti, minima, esse blanditiis iste, provident quaerat illum minus molestiae cum eveniet beatae quasi. Temporibus, voluptate. Dolorem inventore sit, esse temporibus libero corporis eaque fugit totam nostrum quis? Consectetur eligendi vitae, velit saepe modi placeat laborum cupiditate iusto illum excepturi odio, natus nobis quis dicta, soluta cum quae similique. Doloremque laborum quod repellat nulla.</p>
            </div>
        </div>
    </div>
    
    
</section>

<?php get_footer();
